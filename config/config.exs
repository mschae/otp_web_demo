# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

# Configures the endpoint
config :otp_web_demo, OtpWebDemoWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "WFX1FAc0zvFZSEavQ8iCrS2uaYipf1BUvSe0m9TX2vLiKaCIQEN1tP6HKDRDTSYr",
  render_errors: [view: OtpWebDemoWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: OtpWebDemo.PubSub, adapter: Phoenix.PubSub.PG2],
  live_view: [
    signing_salt: "ZjWnlAKNkBmp8JqjQ5rhQ5wzGvmAhkj3"
  ]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
