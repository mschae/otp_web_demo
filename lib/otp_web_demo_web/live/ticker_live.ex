defmodule OtpWebDemoWeb.TickerLive do
  use Phoenix.LiveView

  def render(assigns) do
    OtpWebDemoWeb.PageView.render("index.html", assigns)
  end

  def mount(_session, socket) do
    OtpWebDemo.PriceFetcher.register(self())

    {:ok, assign(socket, price: OtpWebDemo.PriceFetcher.get_price(), highlight: false)}
  end

  def handle_info({:new_price, price}, socket) do
    new_price = price != socket.assigns.price

    :timer.send_after(1_000, :remove_class)
    {:noreply, assign(socket, price: price, highlight: true)}
  end

  def handle_info(:remove_class, socket) do
    {:noreply, assign(socket, highlight: false)}
  end
end
