defmodule OtpWebDemoWeb.PageController do
  use OtpWebDemoWeb, :controller

  def index(conn, _params) do
    price = OtpWebDemo.PriceFetcher.get_price()

    render(conn, "index.html", price: price)
  end
end
