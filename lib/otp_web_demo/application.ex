defmodule OtpWebDemo.Application do
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      OtpWebDemoWeb.Endpoint,
      OtpWebDemoWeb.PriceFetcher
    ]

    opts = [strategy: :one_for_one, name: OtpWebDemo.Supervisor]
    Supervisor.start_link(children, opts)
  end

  def config_change(changed, _new, removed) do
    OtpWebDemoWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
