defmodule OtpWebDemo.PriceFetcher do
  use WebSockex

  require Logger

  @url "https://www.bitstamp.net/api/v2/ticker/btceur"
  @ws_url "wss://ws.bitstamp.net"

  @spec get_price() :: Integer.t()
  def get_price() do
    with {:ok, %{body: data}} <- HTTPoison.get(@url),
         {:ok, %{"last" => price}} <- Jason.decode(data) do
      price
    else
      _error -> 0
    end
  end

  @spec register(pid) :: :ok
  def register(pid) do
    GenServer.cast(__MODULE__, {:register, pid})
  end

  @spec start_link(any()) :: {:error, any()} | {:ok, pid()}
  def start_link(_state) do
    WebSockex.start_link(@ws_url, __MODULE__, %{subscriptions: []}, name: __MODULE__)
  end

  @impl true
  def handle_connect(_conn, state) do
    Logger.info("Connected: #{inspect(self)}")
    send(self(), :subscribe)

    {:ok, state}
  end

  @impl true
  def handle_info({:"$gen_cast", {:register, pid}}, %{subscriptions: subscriptions} = state) do
    state = %{state | subscriptions: [pid | subscriptions]}

    {:ok, state}
  end

  @impl true
  def handle_info(:subscribe, state) do
    payload =
      Jason.encode!(%{
        event: "bts:subscribe",
        data: %{
          channel: "live_trades_btceur"
        }
      })

    Logger.info("Triggering subscription")

    {:reply, {:text, payload}, state}
  end

  @impl true
  def handle_info(msg, state) do
    Logger.info("Unhandled message: #{inspect(msg)}")
    {:ok, state}
  end

  @impl true
  def handle_frame({:text, data}, %{subscriptions: subscriptions} = state) do
    case Jason.decode(data) do
      {:ok, %{"data" => %{"price" => price}}} ->
        Logger.info("Received new price: #{price}")
        Enum.each(subscriptions, &send(&1, {:new_price, price}))

      {:ok, data} ->
        Logger.info("Received frame: #{inspect(data)}")

      other ->
        Logger.error("Error decoding frame: #{inspect(other)}")
    end

    {:ok, state}
  end

  @impl true
  def handle_disconnect(connection_status_map, state) do
    Logger.error("Disconnected: #{inspect(connection_status_map)}")
    {:ok, state}
  end
end
